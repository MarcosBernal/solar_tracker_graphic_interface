#include "calibrating_light_sensors.h"
#include "ui_calibrating_light_sensors.h"

CalibratingLightSensors::CalibratingLightSensors(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CalibratingLightSensors)
{
    ui->setupUi(this);
}

CalibratingLightSensors::CalibratingLightSensors(QWidget *parent, uint8_t *constants, bool *changed_values) :
    QDialog(parent,Qt::WindowSystemMenuHint | Qt::WindowTitleHint), //| Qt::WindowCloseButtonHint), Removed the cross option from window
    changed_values(changed_values),
    light_spot_mod(constants),
    ui(new Ui::CalibratingLightSensors)
{
    ui->setupUi(this);

    this->setWindowTitle(QString("Calibrating PhotoElectric Sensors"));

    light_spot_mod_backup[0] = light_spot_mod[0];
    light_spot_mod_backup[1] = light_spot_mod[1];
    light_spot_mod_backup[2] = light_spot_mod[2];

    ui->adc_spin_X_1->setRange(0, 250);
    ui->adc_spin_X_1->setSingleStep(10);
    ui->adc_spin_X_1->setValue(light_spot_mod[0]);

    ui->adc_spin_X_2->setRange(0, 250);
    ui->adc_spin_X_2->setSingleStep(10);
    ui->adc_spin_X_2->setValue(light_spot_mod[1]);

    ui->adc_spin_Y_1->setRange(0, 250);
    ui->adc_spin_Y_1->setSingleStep(10);
    ui->adc_spin_Y_1->setValue(light_spot_mod[2]);

    ui->adc_spin_Y_2->setRange(0, 250);
    ui->adc_spin_Y_2->setSingleStep(10);
    ui->adc_spin_Y_2->setValue(light_spot_mod[3]);

    refresf_values();
}

CalibratingLightSensors::~CalibratingLightSensors()
{
    delete ui;
}

void CalibratingLightSensors::on_restore_button_clicked()
{
    light_spot_mod[0] = light_spot_mod_backup[0];
    light_spot_mod[1] = light_spot_mod_backup[1];
    light_spot_mod[2] = light_spot_mod_backup[2];

    ui->adc_spin_X_1->setValue(light_spot_mod[0]);
    ui->adc_spin_X_2->setValue(light_spot_mod[1]);
    ui->adc_spin_Y_1->setValue(light_spot_mod[2]);
    ui->adc_spin_Y_2->setValue(light_spot_mod[3]);
    *changed_values = false;
    this->refresf_values();

}

void CalibratingLightSensors::refresf_values()
{
    ui->adc_value_X_1->display(light_spot_mod[0]);
    ui->adc_value_X_2->display(light_spot_mod[1]);
    ui->adc_value_Y_1->display(light_spot_mod[2]);
    ui->adc_value_Y_2->display(light_spot_mod[3]);

}


void CalibratingLightSensors::on_ok_button_clicked()
{
    light_spot_mod[0] = ui->adc_spin_X_1->value();
    light_spot_mod[1] = ui->adc_spin_X_2->value();
    light_spot_mod[2] = ui->adc_spin_Y_1->value();
    light_spot_mod[3] = ui->adc_spin_Y_2->value();
    *changed_values = true;
    refresf_values();
    close();
}

void CalibratingLightSensors::on_cancel_button_clicked()
{
    on_restore_button_clicked();
    close();
}

void CalibratingLightSensors::on_reset_button_clicked()
{
    light_spot_mod[0] = 0;
    light_spot_mod[1] = 0;
    light_spot_mod[2] = 0;
    light_spot_mod[3] = 0;
    ui->adc_spin_X_1->setValue(light_spot_mod[0]);
    ui->adc_spin_X_2->setValue(light_spot_mod[1]);
    ui->adc_spin_Y_1->setValue(light_spot_mod[2]);
    ui->adc_spin_Y_2->setValue(light_spot_mod[3]);
    refresf_values();
}
