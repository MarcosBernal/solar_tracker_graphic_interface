#ifndef SERIAL_MESSAGES_H
#define SERIAL_MESSAGES_H

#include <QByteArray>

class MessageInSerial
{

public:
    MessageInSerial(unsigned char, unsigned char, unsigned char *);

    // definition
        static const unsigned char SYNC_TYPE;
        static const unsigned char SYNC_LEN_DATA;
        static const unsigned char SYNC_LEN_ALL;

        static const unsigned char ASYN_VALU_TYPE;
        static const unsigned char ASYN_VALU_LENGTH_DATA;
        static const unsigned char ASYN_VALU_LENGTH;

        static const unsigned char ASYN_CODE_TYPE;
        static const unsigned char ASYN_CODE_LENGTH_DATA;
        static const unsigned char ASYN_CODE_LENGTH;

        static const unsigned char ASYN_STATE_TYPE;
        static const unsigned char ASYN_STATE_LENGTH_DATA;
        static const unsigned char ASYN_STATE_LENGTH;

        static const unsigned char END_MESSAGE;



    unsigned char get_type(void);
    unsigned char get_length(void);
    unsigned char* get_data(void);
    QByteArray & get_serialized_message();

private:
    unsigned char type;
    unsigned char length;
    unsigned char data[64];
    QByteArray * byte_array;

};

class SyncMessage : public MessageInSerial
{

public:
    SyncMessage(unsigned char*);

public:
    short encoder_values[2];
    short adc_values[4];
    int adc_channels;
    short pid_value[2];
};

class AsynValuMessage : public MessageInSerial
{

public:
    AsynValuMessage(unsigned char*);
    AsynValuMessage(float* k_pid_values, short* deadband_values, int8_t* sign, uint8_t* light_spot);
    ~AsynValuMessage();

public:
    float k_pid_values[3];
    short deadband_value[2];
    int8_t sign[2];
    uint8_t light_spot_mod[4];
};

class AsynCodeMessage : public MessageInSerial
{

public:
    AsynCodeMessage(unsigned char*);
    AsynCodeMessage(unsigned char type, unsigned char code);

    // definition type
    static const unsigned char INITIALA_PIC;
    static const unsigned char WAITING_TASK;
    static const unsigned char PERFORM_TASK;
    static const unsigned char CLOSING_PIC_;
    static const unsigned char PIC_DEBUG_CH;
    static const unsigned char ERR_PIC_RECE;

public:
    unsigned char code_request;
    unsigned char code_reply;

};

class AsynStateMessage : public MessageInSerial
{

public:
    AsynStateMessage(unsigned char*);
    AsynStateMessage(unsigned char state, short posX = 0, short posY = 0);

    // definition state
    static const unsigned char NONE_CURRENT_STATE;
    static const unsigned char MANUAL_MOVEM_STATE;
    static const unsigned char SEARCH_LIGHT_STATE;
    static const unsigned char TRACK__LIGHT_STATE;
    static const unsigned char CLOSING__PIC_STATE;

    static const short MAX_ENCODER_ROUND;

public:
    unsigned char state;
    short new_pos_X;
    short new_pos_Y;
    void set_new_positions(short pos_X, short pos_Y);

};


struct TemporalValues
{
    short encoder_values[2];
    short adc_values[4];
};

class DataStorage
{

public:
    DataStorage();
    ~DataStorage();
    void add_sync_message(SyncMessage);
    void update_asyn_valu_message(AsynValuMessage);
    void update_current_condition(AsynCodeMessage);
    void update_current_state(AsynStateMessage);


    TemporalValues get_last_temporal_value();
    TemporalValues get_last_but_n_temporal_value(int);

    int get_adc_channels();
    short* get_pid_value();
    float* get_k_pid_values();
    float* get_k_pid_values_mplavc18ver();
    void   update_k_pid_values(float *new_values);
    short* get_deadband_value();
    int8_t* get_engines_sign();
    short  get_current_pic_state();
    short  get_current_pic_condition();
    uint8_t get_last_pic_state();
    uint8_t* get_light_spot_mod();
    void set_last_pic_state(uint8_t state);
    void reset_all_data();

    static const unsigned short TEMP_VALUES_SIZE;

private:

    int temporal_values_idx;
    TemporalValues * encoder_and_adc_values;

    int adc_channels;
    short pid_value[2];
    float k_pid_values[3];
    float k_pid_values_mplavc18ver[3];
    short deadband_value[2];
    int8_t engines_sign[2];
    short current_pic_state;
    short current_pic_condition;
    uint8_t last_pic_state;
    uint8_t light_spot_mod[4];

    void float_mplavc18ver_to_ieee(float* src_pointer, float* dst_pointer);
    void float_ieee_to_mplavc18ver(float* src_pointer, float* dst_pointer);

};


#endif // SERIAL_MESSAGES_H
