#ifndef CALIBRATING_LIGHT_SENSORS_H
#define CALIBRATING_LIGHT_SENSORS_H

#include <QDialog>

namespace Ui {
class CalibratingLightSensors;
}

class CalibratingLightSensors : public QDialog
{
    Q_OBJECT

public:
    explicit CalibratingLightSensors(QWidget *parent = 0);
    CalibratingLightSensors(QWidget *parent, uint8_t *constants, bool *changed_values);
    ~CalibratingLightSensors();

private slots:
    void refresf_values();

    void on_restore_button_clicked();

    void on_ok_button_clicked();

    void on_cancel_button_clicked();

    void on_reset_button_clicked();

private:
    bool * changed_values;
    uint8_t * light_spot_mod;
    uint8_t light_spot_mod_backup[4];
    Ui::CalibratingLightSensors *ui;



};

#endif // CALIBRATING_LIGHT_SENSORS_H
