#include <QDebug>

#include "serial_messages.h"

const unsigned char MessageInSerial::SYNC_TYPE = 0xA0; //0b10100000
const unsigned char MessageInSerial::SYNC_LEN_DATA = 0x12; //18;
const unsigned char MessageInSerial::SYNC_LEN_ALL = (MessageInSerial::SYNC_LEN_DATA + 3); //21

const unsigned char MessageInSerial::ASYN_VALU_TYPE = 0xA4;  //0b101001XX
const unsigned char MessageInSerial::ASYN_VALU_LENGTH_DATA = 0x16; //0b00010110 // 22
const unsigned char MessageInSerial::ASYN_VALU_LENGTH = (MessageInSerial::ASYN_VALU_LENGTH_DATA + 3); // 3-> (start, length, stop) 21 ->(vars + meta)

const unsigned char MessageInSerial::ASYN_CODE_TYPE = 0xA8;  //0b101010XX
const unsigned char MessageInSerial::ASYN_CODE_LENGTH_DATA = 0x02; //0b00000010 // 2
const unsigned char MessageInSerial::ASYN_CODE_LENGTH = (MessageInSerial::ASYN_CODE_LENGTH_DATA + 3); // 3-> (start, length, stop) 5 ->(vars + meta)

const unsigned char MessageInSerial::ASYN_STATE_TYPE = 0xA2;  //0b1010001X
const unsigned char MessageInSerial::ASYN_STATE_LENGTH_DATA = 0x05; //0b00000010 // 2
const unsigned char MessageInSerial::ASYN_STATE_LENGTH = (MessageInSerial::ASYN_STATE_LENGTH_DATA + 3); // 3-> (start, length, stop) 5 ->(vars + meta)

const unsigned char MessageInSerial::END_MESSAGE = 0xFF; //255;

const unsigned char AsynCodeMessage::INITIALA_PIC = 0xA8;  //0b10101000
const unsigned char AsynCodeMessage::WAITING_TASK = 0xA9;  //0b10101001
const unsigned char AsynCodeMessage::PERFORM_TASK = 0xAA;  //0b10101010
const unsigned char AsynCodeMessage::PIC_DEBUG_CH = 0xAD;  //0b10101101
const unsigned char AsynCodeMessage::CLOSING_PIC_ = 0xAE;  //0b10101110
const unsigned char AsynCodeMessage::ERR_PIC_RECE = 0xAF;  //0b10101111

const unsigned char AsynStateMessage::NONE_CURRENT_STATE = 0x00;
const unsigned char AsynStateMessage::MANUAL_MOVEM_STATE = 0x01;
const unsigned char AsynStateMessage::SEARCH_LIGHT_STATE = 0x02;
const unsigned char AsynStateMessage::TRACK__LIGHT_STATE = 0x03;
const unsigned char AsynStateMessage::CLOSING__PIC_STATE = 0x04;

const short AsynStateMessage::MAX_ENCODER_ROUND = 564;

const unsigned short DataStorage::TEMP_VALUES_SIZE = 1024;


MessageInSerial::MessageInSerial(unsigned char type, unsigned char length, unsigned char *data)
{
    this->type = type;
    this->length = length;
    memcpy(this->data, data, this->length+3);
}

unsigned char * MessageInSerial::get_data()
{
    return this->data;
}

unsigned char MessageInSerial::get_type()
{
    return this->type;
}

unsigned char MessageInSerial::get_length()
{
    return this->length;
}

QByteArray & MessageInSerial::get_serialized_message()
{
    this->byte_array = new QByteArray((char *)this->data, this->get_length()+3);
    for(int i = 0; i < this->byte_array->size();i++)
        if(this->byte_array->data()[i] == ((int8_t)0) || ((int8_t)this->byte_array->data()[i]) == ((int8_t)250))
            this->byte_array->data()[i] = (this->byte_array->data()[i] == 0) ? 250 : 251;

    return *(this->byte_array);
}

SyncMessage::SyncMessage(unsigned char *text)
    :MessageInSerial(text[0], text[1], text)
{
    if(this->get_length() > MessageInSerial::SYNC_LEN_DATA)
    {
        qDebug() << "Error al crear mensage serial" << text;
        return;
    }
    else if(this->get_data()[(MessageInSerial::SYNC_LEN_ALL-1)] != MessageInSerial::END_MESSAGE)
    {
        qDebug() << "Final distinto: Longitud" << int(text[1]) <<  " del mensaje erronea. Recivido" << this->get_data()[(MessageInSerial::SYNC_LEN_ALL-1)] << "-"
         << text[0] << " " << text[1] << " " << text[2] << " " << text[3] << " " << text[4] << " " << text[5] << " " << text[6] << " " << text[7] << " "
         << text[8] << " " << text[9] << " " << text[10] << " " << text[11] << " " << text[12] << " " << text[13] << " " << text[14] << " " << text[15] << " "
         << text[16] << " " << text[17] << " " << text[18] << " " << text[19] << " " << text[20] << " " << text[21] << " " << text[22] << " " << text[23] << " "
         << text[24];
        return;
    }

    unsigned char *data = &(this->get_data())[2];

    this->encoder_values[0] = (data[1]<<8)+data[0];
    this->encoder_values[1] = (data[3]<<8)+data[2];

    this->adc_values[0] = (data[5]<<8)+data[4];
    this->adc_values[1] = (data[7]<<8)+data[6];
    this->adc_values[2] = (data[9]<<8)+data[8];
    this->adc_values[3] = (data[11]<<8)+data[10];

    this->adc_channels = data[12];
    //data[13] Reserved

    this->pid_value[0] = (data[15]<<8)+data[14];
    this->pid_value[1] = (data[17]<<8)+data[16];


    /*qDebug() << "Menasaje sincrono: Longitud" << int(text[1]) <<  " del mensaje erronea. Recivido" << this->get_data()[(MessageInSerial::SYNC_LEN_ALL-1)] << "-"
     << text[0] << " " << text[1] << " " << text[2] << " " << text[3] << " " << text[4] << " " << text[5] << " " << text[6] << " " << text[7] << " "
     << text[8] << " " << text[9] << " " << text[10] << " " << text[11] << " " << text[12] << " " << text[13] << " " << text[14] << " " << text[15] << " "
     << text[16] << " " << text[17] << " " << text[18] << " " << text[19] << " " << text[20] << " " << text[21] << " " << text[22] << " " << text[23] << " "
     << text[24];*/

}

AsynValuMessage::AsynValuMessage(unsigned char* text)
    :MessageInSerial(text[0], text[1], text)
{
    if(this->get_length() > MessageInSerial::ASYN_VALU_LENGTH_DATA)
    {
        qDebug() << "Error al crear mensage serial" << text;
        return;
    }
    else if(this->get_data()[(MessageInSerial::ASYN_VALU_LENGTH-1)] != MessageInSerial::END_MESSAGE)
    {
        qDebug() << "Final distinto: Longitud" << int(text[1]) <<  " del mensaje erronea. Recivido" << this->get_data()[(MessageInSerial::ASYN_VALU_LENGTH-1)] << "-"
         << text[0] << " " << text[1] << " " << text[2] << " " << text[3] << " " << text[4] << " " << text[5] << " " << text[6] << " " << text[7] << " "
         << text[8] << " " << text[9] << " " << text[10] << " " << text[11] << " " << text[12] << " " << text[13] << " " << text[14] << " " << text[15] << " "
         << text[16] << " " << text[17] << " " << text[18] << " " << text[19] << " " << text[20] << " " << text[21] << " " << text[22] << " " << text[23];
        return;
    }

    unsigned char *data = &(this->get_data())[2];

    uint8_t float_bytes[12];
    for(int i = 0; i < 12;i++)
        float_bytes[i] = data[i];

    memcpy(((uint8_t *)&k_pid_values[0]), float_bytes, sizeof(float));
    memcpy(((uint8_t *)&k_pid_values[1]), &float_bytes[4], sizeof(float));
    memcpy(((uint8_t *)&k_pid_values[2]), &float_bytes[8], sizeof(float));

    this->deadband_value[0] = (data[13]<<8)+data[12];
    this->deadband_value[1] = (data[15]<<8)+data[14];

    this->sign[0] = data[16];
    this->sign[1] = data[17];

    this->light_spot_mod[0] = data[18];
    this->light_spot_mod[1] = data[19];
    this->light_spot_mod[2] = data[20];
    this->light_spot_mod[3] = data[21];

    qDebug() << "Mensaje Asyn Valu: Longitud" << int(text[1]) <<  " del mensaje "
         << text[0] << " " << text[1] << " " << text[2] << " " << text[3] << " " << text[4] << " " << text[5] << " " << text[6] << " " << text[7] << " "
         << text[8] << " " << text[9] << " " << text[10] << " " << text[11] << " " << text[12] << " " << text[13] << " " << text[14] << " " << text[15] << " "
         << text[16] << " " << text[17] << " " << text[18]  << " " << text[19] << " " << text[20] << " " << text[21] << " " << text[22] << " " << text[23] << " "
         << text[24] << " " << text[25];


}

AsynValuMessage::AsynValuMessage(float* k_pid_values, short* deadband_values, int8_t* sign, uint8_t* light_spot)
    :MessageInSerial(MessageInSerial::ASYN_VALU_TYPE, MessageInSerial::ASYN_VALU_LENGTH_DATA, new unsigned char[MessageInSerial::ASYN_VALU_LENGTH]())
{
    unsigned char *text = this->get_data();
    text[0] = MessageInSerial::ASYN_VALU_TYPE;
    text[1] = MessageInSerial::ASYN_VALU_LENGTH_DATA;
    text[MessageInSerial::ASYN_VALU_LENGTH-1] = MessageInSerial::END_MESSAGE;

    memcpy(&text[2], ((unsigned char*)k_pid_values), sizeof(float)*3);
    memcpy(&text[14], ((unsigned char*)deadband_values), sizeof(short)*2);
    memcpy(&text[20], ((unsigned char*)light_spot), sizeof(uint8_t)*4);

    this->k_pid_values[0] = k_pid_values[0];
    this->k_pid_values[1] = k_pid_values[1];
    this->k_pid_values[2] = k_pid_values[2];
    this->deadband_value[0] = deadband_values[0];
    this->deadband_value[1] = deadband_values[1];
    this->sign[0] = sign[0];
    this->sign[1] = sign[1];

    this->light_spot_mod[0] = light_spot[0];
    this->light_spot_mod[1] = light_spot[1];
    this->light_spot_mod[2] = light_spot[2];
    this->light_spot_mod[3] = light_spot[3];

    qDebug() << "Light spot mod" << light_spot_mod[0] << " " << light_spot_mod[1] << " " << light_spot_mod[2] << " " << light_spot_mod[3];
}

AsynValuMessage::~AsynValuMessage()
{
    //free(this->k_pid_values);
}

AsynCodeMessage::AsynCodeMessage(unsigned char* text)
    :MessageInSerial(text[0], text[1], text)
{
    if(this->get_length() > MessageInSerial::ASYN_CODE_LENGTH_DATA)
    {
        qDebug() << "Error to build -AsynCodeMessage instance-: Length " << this->get_length()  << "higher than expected. And original lenght" << text[1];
        return;
    }
    else if(this->get_data()[(MessageInSerial::ASYN_CODE_LENGTH-1)] != MessageInSerial::END_MESSAGE)
    {
        qDebug() << "Final distinto: Longitud" << int(text[1]) <<  " del mensaje erronea. Recivido" << this->get_data()[(MessageInSerial::ASYN_CODE_LENGTH-1)] << "-"
         << text[0] << " " << text[1] << " " << text[2] << " " << text[3] << " " << text[4] << " " << text[5];
        return;
    }

    unsigned char *data = &(this->get_data())[2];

    this->code_request = data[0];
    this->code_reply = data[1];

    qDebug() << "Mensaje Asyn Code: Longitud" << int(text[1]) <<  " del mensaje "
         << text[0] << " " << text[1] << " " << text[2] << " " << text[3] << " " << text[4] << " " << text[5] << " " << text[6];

}

AsynCodeMessage::AsynCodeMessage(unsigned char type, unsigned char code)
    :MessageInSerial(MessageInSerial::ASYN_CODE_TYPE, MessageInSerial::ASYN_CODE_LENGTH_DATA, new unsigned char[MessageInSerial::ASYN_CODE_LENGTH]())
{
    unsigned char * text = this->get_data();
    text[0] = MessageInSerial::ASYN_CODE_TYPE;
    text[1] = MessageInSerial::ASYN_CODE_LENGTH_DATA;
    text[2] = type;
    text[3] = code;
    text[4] = MessageInSerial::END_MESSAGE;

    this->code_request = text[2];
    this->code_reply = text[3];

    if(this->get_length() > MessageInSerial::ASYN_CODE_LENGTH_DATA)
    {
        qDebug() << "Error to build -AsynCodeMessage instance-: Length " << this->get_length()  << "higher than expected. And original lenght" << text[1];
        return;
    }

}


AsynStateMessage::AsynStateMessage(unsigned char* text)
    :MessageInSerial(text[0], text[1], text)
{
    if(this->get_length() > MessageInSerial::ASYN_STATE_LENGTH_DATA)
    {
        qDebug() << "Error to build -AsynCodeMessage instance-: Length " << this->get_length()  << "higher than expected. And original lenght" << text[1];
        return;
    }
    else if(this->get_data()[(MessageInSerial::ASYN_STATE_LENGTH-1)] != MessageInSerial::END_MESSAGE)
    {
        qDebug() << "Final distinto: Longitud" << int(text[1]) <<  " del mensaje erronea. Recivido" << this->get_data()[(MessageInSerial::SYNC_LEN_ALL-1)] << "-"
         << text[0] << " " << text[1] << " " << text[2] << " " << text[3] << " " << text[4] << " " << text[5];
        return;
    }

    unsigned char *data = &(this->get_data())[2];

    this->state = data[0];

     if(state == AsynStateMessage::MANUAL_MOVEM_STATE)
     {
         this->new_pos_X = (data[2]<<8)+data[1];
         this->new_pos_Y = (data[4]<<8)+data[3];
     }
     else
     {
         this->new_pos_X = 0;
         this->new_pos_Y = 0;
     }
}

AsynStateMessage::AsynStateMessage(unsigned char state, short posX, short posY) //posX, posY = 0 by default
    :MessageInSerial(MessageInSerial::ASYN_STATE_TYPE, MessageInSerial::ASYN_STATE_LENGTH_DATA, new unsigned char[MessageInSerial::ASYN_STATE_LENGTH]())
{
    unsigned char * text = this->get_data();
    text[0] = MessageInSerial::ASYN_STATE_TYPE;
    text[1] = MessageInSerial::ASYN_STATE_LENGTH_DATA;
    text[2] = state;

    this->new_pos_X = (posX >= 180) ? ((float)(posX-180)/180)*AsynStateMessage::MAX_ENCODER_ROUND : -(((float)(180-posX)/180)*AsynStateMessage::MAX_ENCODER_ROUND);
    this->new_pos_Y = (posY >= 180) ? ((float)(posY-180)/180)*AsynStateMessage::MAX_ENCODER_ROUND : -(((float)(180-posY)/180)*AsynStateMessage::MAX_ENCODER_ROUND);

    memcpy(&text[3], ((unsigned char*)&this->new_pos_X), sizeof(short));
    memcpy(&text[5], ((unsigned char*)&this->new_pos_Y), sizeof(short));

    text[MessageInSerial::ASYN_STATE_LENGTH-1] = MessageInSerial::END_MESSAGE;

    if(this->get_length() > MessageInSerial::ASYN_STATE_LENGTH_DATA)
    {
        qDebug() << "Error to build -AsynCodeMessage instance-: Length " << this->get_length()  << "higher than expected. And original lenght" << text[1];
        return;
    }


    qDebug() << "Construido mensaje con" << this->new_pos_X <<" y " << this->new_pos_Y;
}




DataStorage::DataStorage()
{
 this->reset_all_data();
}

void DataStorage::add_sync_message(SyncMessage message)
{
    this->adc_channels = message.adc_channels;

    this->pid_value[0] = message.pid_value[0];
    this->pid_value[1] = message.pid_value[1];

    int j = 0;
    for(j = 0; j < 4; j++)
    {
        if(j < 2)
            this->encoder_and_adc_values[this->temporal_values_idx].encoder_values[j] = message.adc_values[j];
        this->encoder_and_adc_values[this->temporal_values_idx].adc_values[j] = message.encoder_values[j];

    }

    this->temporal_values_idx = (this->temporal_values_idx + 1) & (DataStorage::TEMP_VALUES_SIZE -1);
}

void DataStorage::update_asyn_valu_message(AsynValuMessage message)
{
    this->deadband_value[0] = message.deadband_value[0];
    this->deadband_value[1] = message.deadband_value[1];

    this->k_pid_values_mplavc18ver[0] = message.k_pid_values[0];
    this->k_pid_values_mplavc18ver[1] = message.k_pid_values[1];
    this->k_pid_values_mplavc18ver[2] = message.k_pid_values[2];

    this->engines_sign[0] = message.sign[0];
    this->engines_sign[1] = message.sign[1];

    this->light_spot_mod[0] = message.light_spot_mod[0];
    this->light_spot_mod[1] = message.light_spot_mod[1];
    this->light_spot_mod[2] = message.light_spot_mod[2];
    this->light_spot_mod[3] = message.light_spot_mod[3];

    float_mplavc18ver_to_ieee(this->k_pid_values_mplavc18ver, this->k_pid_values);
}

void DataStorage::update_current_condition(AsynCodeMessage message)
{
    this->current_pic_condition = message.code_request;
}

void DataStorage::update_current_state(AsynStateMessage message)
{
    this->current_pic_state = message.state;
}

struct TemporalValues DataStorage::get_last_temporal_value()
{
    if(this->temporal_values_idx == 0)
        return this->encoder_and_adc_values[DataStorage::TEMP_VALUES_SIZE - 1];
    else
        return this->encoder_and_adc_values[ this->temporal_values_idx - 1];
}

struct TemporalValues DataStorage::get_last_but_n_temporal_value(int idx)
{
    if((this->temporal_values_idx - idx) == 0)
        return this->encoder_and_adc_values[DataStorage::TEMP_VALUES_SIZE - 1];
    else
        return this->encoder_and_adc_values[(this->temporal_values_idx - idx) - 1];
}

int DataStorage::get_adc_channels()
{
    return this->adc_channels;
}

short* DataStorage::get_pid_value()
{
    return this->pid_value;
}

float* DataStorage::get_k_pid_values()
{
    return this->k_pid_values;
}

float* DataStorage::get_k_pid_values_mplavc18ver()
{
    return this->k_pid_values_mplavc18ver;
}

uint8_t* DataStorage::get_light_spot_mod()
{
    return this->light_spot_mod;
}

void DataStorage::update_k_pid_values(float *new_values)
{
    memcpy(this->k_pid_values, new_values, sizeof(float)*3);
    this->float_ieee_to_mplavc18ver(this->k_pid_values, this->k_pid_values_mplavc18ver);
    return;
}

void DataStorage::float_mplavc18ver_to_ieee(float* src_pointer, float* dst_pointer)
{
    memcpy(dst_pointer, src_pointer, sizeof(float)*3);
    uint8_t higher_byte;
    uint8_t lower_byte;

    uint8_t *float_pointer = reinterpret_cast<uint8_t*>(dst_pointer);

    for(int i = 0; i < 3; i++)
    {
        higher_byte = float_pointer[0+i*4];
        lower_byte = float_pointer[1+i*4];

        float_pointer[0+i*4] = (higher_byte >> 1 & 0x7F) | (lower_byte & 0x80);
        float_pointer[1+i*4] = (lower_byte & 0x7F) | (higher_byte << 7 & 0x80);
    }
}

void DataStorage::float_ieee_to_mplavc18ver(float* src_pointer, float* dst_pointer)
{
    memcpy(dst_pointer, src_pointer, sizeof(float)*3);
    uint8_t higher_byte;
    uint8_t lower_byte;

    uint8_t *float_pointer = reinterpret_cast<uint8_t*>(dst_pointer);

    for(int i = 0; i < 3; i++)
    {
        higher_byte = float_pointer[0+i*4];
        lower_byte = float_pointer[1+i*4];

        float_pointer[0+i*4] = (higher_byte << 1 & 0xFE) | ((lower_byte  >> 7)& 0x01);
        float_pointer[1+i*4] = (lower_byte & 0x7F) | (higher_byte & 0x80);
    }
}

short* DataStorage::get_deadband_value()
{
    return this->deadband_value;
}

int8_t* DataStorage::get_engines_sign()
{
    return this->engines_sign;
}

short DataStorage::get_current_pic_state()
{
    return this->current_pic_state;
}

short DataStorage::get_current_pic_condition()
{
    return this->current_pic_condition;
}

void DataStorage::reset_all_data()
{
    this->adc_channels = 0;

    this->deadband_value[0] = 0;
    this->deadband_value[1] = 0;

    this->engines_sign[0] = 0;
    this->engines_sign[1] = 0;

    this->k_pid_values[0] = 0;
    this->k_pid_values[1] = 0;
    this->k_pid_values[2] = 0;

    this->pid_value[0] = 0;
    this->pid_value[1] = 0;
    this->current_pic_state = 0;
    this->last_pic_state = 0;

    this->temporal_values_idx = 0;
    this->encoder_and_adc_values = new TemporalValues[TEMP_VALUES_SIZE];

    for(int i = 0; i < TEMP_VALUES_SIZE; i++)
        for(int j = 0; j < 4; j++)
        {
            if(j < 2)
                encoder_and_adc_values[i].encoder_values[j] = 0;
            encoder_and_adc_values[i].adc_values[j] = 0;

        }

    this->light_spot_mod[0] = 0;
    this->light_spot_mod[1] = 0;
    this->light_spot_mod[2] = 0;
    this->light_spot_mod[3] = 0;
}


uint8_t DataStorage::get_last_pic_state()
{
    return this->last_pic_state;
}

void DataStorage::set_last_pic_state(uint8_t state)
{
    this->last_pic_state = state;
}
