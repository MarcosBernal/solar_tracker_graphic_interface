#include "connected_pic_dialog.h"
#include "ui_connected_pic_dialog.h"
#include "calibrating_engines.h"
#include "calibrating_light_sensors.h"

ConnectedPICDialog::ConnectedPICDialog(QWidget *parent, QSerialPortInfo *info_conn, QSerialPort *serial_conn) :
    QDialog(parent,Qt::WindowSystemMenuHint | Qt::WindowTitleHint | Qt::WindowCloseButtonHint),
    serial(serial_conn),
    info(info_conn),
    received_data_storage(new DataStorage),
    ui(new Ui::ConnectedPICDialog)
{
    ui->setupUi(this);

    setWindowTitle(QString("Connecting with PIC18F4520"));
    reseting_graphical_interface();

    //QPixmap pix(":/MyPictures/state_machine_3_red.png");
    //pix.scaled(ui->state_machine_representation->geometry().width(), ui->state_machine_representation->geometry().height(), Qt::IgnoreAspectRatio); // Returns a copy of the image

    //ui->state_machine_representation->setPixmap(pix.scaled(ui->state_machine_representation->geometry().width(), ui->state_machine_representation->geometry().height(), Qt::IgnoreAspectRatio));
    //ui->state_machine_representation->setFixedSize(ui->state_machine_representation->geometry().width(), ui->state_machine_representation->geometry().height());

    serial_output = ui->serial_output;
    serial_protocol_activated = false;

    connect(serial, static_cast<void (QSerialPort::*)(QSerialPort::SerialPortError)>(&QSerialPort::error),
            this, &ConnectedPICDialog::handleError);

    connect(serial, &QSerialPort::readyRead, this, &ConnectedPICDialog::readDataSerial);
    connect(serial_output, &Console::getData, this, &ConnectedPICDialog::writeDataSerial);
}

ConnectedPICDialog::~ConnectedPICDialog()
{
    disconnect(serial, static_cast<void (QSerialPort::*)(QSerialPort::SerialPortError)>(&QSerialPort::error),this, &ConnectedPICDialog::handleError);
    disconnect(serial, &QSerialPort::readyRead, this, &ConnectedPICDialog::readDataSerial);
    disconnect(serial_output, &Console::getData, this, &ConnectedPICDialog::writeDataSerial);
    closeSerialPort();
    delete ui;

}

void ConnectedPICDialog::closeSerialPort()
{
    if (serial->isOpen())
        serial->close();
    serial_output->setEnabled(false);
}

void ConnectedPICDialog::writeDataSerial(const QByteArray &data)
{

    for(int i = 0; i < data.size(); i++)
        serial->write(&data.data()[i], 1);

    if(data.size() <= 5)
        qDebug() << "Send "<< QString::number(((uint8_t)data.data()[0])) << " " << QString::number(((uint8_t)data.data()[1])) << " " <<
                              QString::number(((uint8_t)data.data()[2])) << " " << QString::number(((uint8_t)data.data()[3])) << " " <<
                              QString::number(((uint8_t)data.data()[4]));
    else
        qDebug() << "Send "<< data.data() << data.size();
}

void ConnectedPICDialog::readDataSerial()
{
    QByteArray received_data("");

    while (!serial->atEnd())
        received_data.append(serial->read(1).data());

     manageDataSerial(received_data);
}


int buffer_idx = 0;
unsigned char message_char [128];


void ConnectedPICDialog::sort_message_bytes(unsigned char message_char[])
{
    int i = 0;
    int j = 0;
    bool message_header = false;
    for(i = 1; (i < buffer_idx)&&!message_header; i++)
    {
        if((message_char[i] == MessageInSerial::SYNC_TYPE && message_char[i+1] == MessageInSerial::SYNC_LEN_DATA)||
                (message_char[i] == MessageInSerial::ASYN_VALU_TYPE && message_char[i+1] == MessageInSerial::ASYN_VALU_LENGTH_DATA)||
                    (message_char[i] == MessageInSerial::ASYN_CODE_TYPE && message_char[i+1] == MessageInSerial::ASYN_CODE_LENGTH_DATA)||
                        (message_char[i] == MessageInSerial::ASYN_STATE_TYPE && message_char[i+1] == MessageInSerial::ASYN_STATE_LENGTH_DATA))
            message_header = true;
    }

    if(message_header)
    {
        for(j = 0; i < buffer_idx; j++)
        {
            message_char[j] = message_char[i];
            message_char[i++] = 0;
        }

        buffer_idx = j;
        qDebug() << "Ordenado mensaje";
    }
    else
        buffer_idx = 0;

}

void ConnectedPICDialog::manageDataSerial(QByteArray received_data)
{
   for(int i = 0; i < received_data.size() ;i++)
      {           
          message_char[buffer_idx] = (((unsigned char)(received_data.data()[i]) == 250)) ? 0 : received_data.data()[i];
          buffer_idx = (buffer_idx==127) ? 0 : buffer_idx+1;
      }
      if(buffer_idx > message_char[1]+3 || buffer_idx > 54)
      {
         //MessageInSerial *new_message;

         if(message_char[0] == MessageInSerial::SYNC_TYPE && message_char[MessageInSerial::SYNC_LEN_ALL-1] == MessageInSerial::END_MESSAGE)
         {
            SyncMessage message(message_char);
            manageSyncMessageStorage(message);
         }
         else if(message_char[0] == MessageInSerial::ASYN_VALU_TYPE && message_char[MessageInSerial::ASYN_VALU_LENGTH-1] == MessageInSerial::END_MESSAGE)
         {
            AsynValuMessage message(message_char);
            manageAsynValuMessageStorage(message);
         }
         else if(message_char[0] == MessageInSerial::ASYN_CODE_TYPE && message_char[MessageInSerial::ASYN_CODE_LENGTH-1] == MessageInSerial::END_MESSAGE)
         {
            AsynCodeMessage message(message_char);
            manageAsynCodeMessageStorage(message);
         }
         else if(message_char[0] == MessageInSerial::ASYN_STATE_TYPE && message_char[MessageInSerial::ASYN_STATE_LENGTH-1] == MessageInSerial::END_MESSAGE)
         {
            AsynStateMessage message(message_char);
            manageAsynStateMessageStorage(message);
         }
         else
         {
             qDebug() << "Received unexpected message type";
             sort_message_bytes(message_char);
             return;
         }

         int end_buffer_idx = buffer_idx;
         int message_size = (message_char[1]+3);

         for(buffer_idx = 0; (message_size+buffer_idx) < end_buffer_idx; buffer_idx++)
             message_char[buffer_idx] = message_char[message_size+buffer_idx];

      }
}


void ConnectedPICDialog::manageAsynValuMessageStorage(AsynValuMessage message_serial)
{
  this->received_data_storage->update_asyn_valu_message(message_serial);
  ui->deadband_1->display(this->received_data_storage->get_deadband_value()[0]);
  ui->deadband_2->display(this->received_data_storage->get_deadband_value()[1]);
  ui->kpidvalues_1->display(this->received_data_storage->get_k_pid_values()[0]);
  ui->kpidvalues_2->display(this->received_data_storage->get_k_pid_values()[1]);
  ui->kpidvalues_3->display(this->received_data_storage->get_k_pid_values()[2]);

  if(this->received_data_storage->get_engines_sign()[0] == ((int8_t)1))
      ui->engine_sign_X->setText("<font color='green'>+</font>");
  else if(this->received_data_storage->get_engines_sign()[0] == ((int8_t)-1))
      ui->engine_sign_X->setText("<font color='red'>-</font>");
  else
      ui->engine_sign_X->setText("<font color='grey'>|</font>");

  if(this->received_data_storage->get_engines_sign()[1] == ((int8_t)1))
      ui->engine_sign_Y->setText("<font color='green'>+</font>");
  else if(this->received_data_storage->get_engines_sign()[1] == ((int8_t)-1))
      ui->engine_sign_Y->setText("<font color='red'>-</font>");
  else
      ui->engine_sign_Y->setText("<font color='grey'>|</font>");
}


void ConnectedPICDialog::manageAsynStateMessageStorage(AsynStateMessage message_serial)
{
    if(message_serial.state == AsynStateMessage::NONE_CURRENT_STATE )
      ui->PIC_state_text->setText(QString("NONE_CURRENT_STATE"));
    else if(message_serial.state == AsynStateMessage::MANUAL_MOVEM_STATE )
      ui->PIC_state_text->setText(QString("MANUAL_MOVEM_STATE"));
    else if(message_serial.state == AsynStateMessage::SEARCH_LIGHT_STATE)
      ui->PIC_state_text->setText(QString("SEARCH_LIGHT_STATE"));
    else if(message_serial.state == AsynStateMessage::TRACK__LIGHT_STATE)
      ui->PIC_state_text->setText(QString("TRACK__LIGHT_STATE"));
    else if(message_serial.state == AsynStateMessage::CLOSING__PIC_STATE)
      ui->PIC_state_text->setText(QString("CLOSING__PIC_STATE"));

}

void ConnectedPICDialog::manageAsynCodeMessageStorage(AsynCodeMessage message_serial)
{
    if(message_serial.code_request != AsynCodeMessage::PIC_DEBUG_CH)
        this->received_data_storage->update_current_condition(message_serial);

    if(message_serial.code_request == AsynCodeMessage::WAITING_TASK )
      ui->PIC_condition_text->setText(QString("Waiting for a Task"));
    else if(message_serial.code_request == AsynCodeMessage::CLOSING_PIC_ )
      ui->PIC_condition_text->setText(QString("Closing comunication with PIC"));
    else if(message_serial.code_request == AsynCodeMessage::INITIALA_PIC)
      ui->PIC_condition_text->setText(QString("Initializing PIC"));
    else if(message_serial.code_request == AsynCodeMessage::PERFORM_TASK)
      ui->PIC_condition_text->setText(QString("Performing Task"));
    else if(message_serial.code_request == AsynCodeMessage::ERR_PIC_RECE)
    {
        ui->PIC_condition_text->setText(QString("PIC Error receiving Async message"));
        qDebug() << "Recibido error";
    }
    else if(message_serial.code_request == AsynCodeMessage::PIC_DEBUG_CH)
        qDebug() << "Debug recuvido " << message_serial.code_reply;
    else
        qDebug() << "Recibido mensaje inesperado";
}

void ConnectedPICDialog::manageSyncMessageStorage(SyncMessage message_serial)
{
    if(message_serial.get_type() != MessageInSerial::SYNC_TYPE)
    {
        qDebug() << "Error in manageSyncMessageStorage()";
        return;
    }

    //SyncMessage * message_serial_temp = ((SyncMessage *)message_serial);
    ui->adc_value_X_1->display(message_serial.adc_values[0]);
    ui->adc_value_X_2->display(message_serial.adc_values[1]);
    ui->adc_value_Y_1->display(message_serial.adc_values[2]);
    ui->adc_value_Y_2->display(message_serial.adc_values[3]);
    ui->encoder_X->display(message_serial.encoder_values[0]);
    ui->encoder_Y->display(message_serial.encoder_values[1]);
    ui->last_pid_X->display(message_serial.pid_value[0]);
    ui->last_pid_Y->display(message_serial.pid_value[1]);

    this->received_data_storage->add_sync_message(message_serial);


}
void ConnectedPICDialog::handleError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError) {
        QMessageBox::critical(this, tr("Critical Error"), serial->errorString());
        closeSerialPort();
    }
}

void ConnectedPICDialog::on_calibrate_k_pid_values_clicked()
{
    bool changed_values = false;
    CalibratingEngines calibrating_window(this, this->received_data_storage->get_k_pid_values(), &changed_values);
    calibrating_window.exec();

    if(changed_values)
    {
        AsynValuMessage update_message(this->received_data_storage->get_k_pid_values(), this->received_data_storage->get_deadband_value(), this->received_data_storage->get_engines_sign(),
                                       this->received_data_storage->get_light_spot_mod());
        QByteArray message_serial = update_message.get_serialized_message();
        this->writeDataSerial(message_serial);
        qDebug() << "Mandada actualizacion de k_pid";
    }
}

void ConnectedPICDialog::send_new_state_message(AsynStateMessage message)
{
    uint8_t condition = this->received_data_storage->get_current_pic_condition();
    if(condition == AsynCodeMessage::PERFORM_TASK || condition == AsynCodeMessage::ERR_PIC_RECE)
    {
       QMessageBox::warning(this, "PIC not ready", "Please wait a moment before sending another order");
       return;
    }

    QByteArray message_serial = message.get_serialized_message();
    this->writeDataSerial(message_serial);
    qDebug() << "Mandada estado " + message.state;
    this->received_data_storage->set_last_pic_state(message.state);

    AsynCodeMessage blocking_message(AsynCodeMessage::PERFORM_TASK, 0);
    this->manageAsynCodeMessageStorage(blocking_message);

}

void ConnectedPICDialog::on_search_light_spot_button_clicked()
{
    AsynStateMessage message(AsynStateMessage::SEARCH_LIGHT_STATE);
    send_new_state_message(message);
}

void ConnectedPICDialog::on_track_light_spot_button_clicked()
{
    AsynStateMessage message(AsynStateMessage::TRACK__LIGHT_STATE);
    send_new_state_message(message);
}

void ConnectedPICDialog::on_manual_movement_reset_button_clicked()
{
    ui->manual_movement_axis_x_slider->setValue(180);
    ui->manual_movement_axis_y_slider->setValue(180);
    ui->manual_movement_axis_x_lcd->display(180);
    ui->manual_movement_axis_y_lcd->display(180);
    AsynStateMessage message(AsynStateMessage::MANUAL_MOVEM_STATE, ui->manual_movement_axis_x_slider->value(), ui->manual_movement_axis_y_slider->value());
    send_new_state_message(message);
}

void ConnectedPICDialog::on_manual_movement_init_button_clicked()
{
    AsynStateMessage message(AsynStateMessage::MANUAL_MOVEM_STATE, ui->manual_movement_axis_x_lcd->value(), ui->manual_movement_axis_y_lcd->value());
    send_new_state_message(message);
}

void ConnectedPICDialog::on_close_communication_button_clicked()
{
    AsynStateMessage message(AsynStateMessage::CLOSING__PIC_STATE);
    send_new_state_message(message);
    qDebug() << "Mandada orden FIN";
    reseting_graphical_interface();
}

void ConnectedPICDialog::reseting_graphical_interface()
{
    this->serial_protocol_activated = false;
    this->received_data_storage->reset_all_data();

    ui->adc_value_X_1->display(0);
    ui->adc_value_X_2->display(0);
    ui->adc_value_Y_1->display(0);
    ui->adc_value_Y_2->display(0);

    ui->encoder_X->display(0);
    ui->encoder_Y->display(0);

    ui->last_pid_X->display(0);
    ui->last_pid_Y->display(0);

    ui->deadband_1->display(0);
    ui->deadband_2->display(0);

    ui->kpidvalues_1->display(0);
    ui->kpidvalues_2->display(0);
    ui->kpidvalues_3->display(0);

    ui->engine_sign_X->setText("<font color='grey'>|</font>");
    ui->engine_sign_Y->setText("<font color='grey'>|</font>");

    ui->manual_movement_axis_x_slider->setMaximum(330);
    ui->manual_movement_axis_x_slider->setMinimum(30);
    ui->manual_movement_axis_y_slider->setSingleStep(1);

    ui->manual_movement_axis_y_slider->setMaximum(250);
    ui->manual_movement_axis_y_slider->setMinimum(110);
    ui->manual_movement_axis_y_slider->setSingleStep(1);


    ui->manual_movement_axis_x_slider->setValue(180);
    ui->manual_movement_axis_y_slider->setValue(180);

    ui->manual_movement_axis_x_lcd->display(ui->manual_movement_axis_x_slider->value());
    ui->manual_movement_axis_y_lcd->display(ui->manual_movement_axis_y_slider->value());

    ui->PIC_condition_text->setText(QString("NO connection"));
    ui->PIC_state_text->setText(QString("NONE_CURRENT_STATE"));
    buffer_idx = 0;

    ui->photoelectric_value_label_x_1->setText(QString(""));
    ui->photoelectric_value_label_x_2->setText(QString(""));
    ui->photoelectric_value_label_y_1->setText(QString(""));
    ui->photoelectric_value_label_y_2->setText(QString(""));

}





void ConnectedPICDialog::on_manual_movement_axis_y_slider_sliderReleased()
{
    AsynStateMessage message(AsynStateMessage::MANUAL_MOVEM_STATE, ui->manual_movement_axis_x_slider->value(), ui->manual_movement_axis_y_slider->value());
    send_new_state_message(message);
}

void ConnectedPICDialog::on_manual_movement_axis_x_slider_sliderReleased()
{
    AsynStateMessage message(AsynStateMessage::MANUAL_MOVEM_STATE, ui->manual_movement_axis_x_slider->value(), ui->manual_movement_axis_y_slider->value());
    send_new_state_message(message);
}

void ConnectedPICDialog::on_calibrate_photoelectric_value_clicked()
{
    bool changed_values = false;
    CalibratingLightSensors calibrating_window(this, this->received_data_storage->get_light_spot_mod(), &changed_values);
    calibrating_window.exec();

    if(changed_values)
    {
        AsynValuMessage update_message(this->received_data_storage->get_k_pid_values(), this->received_data_storage->get_deadband_value(), this->received_data_storage->get_engines_sign(),
                                       this->received_data_storage->get_light_spot_mod());
        QByteArray message_serial = update_message.get_serialized_message();
        this->writeDataSerial(message_serial);
        qDebug() << "Mandada actualizacion de photoelectric sensors" << this->received_data_storage->get_light_spot_mod()[0] << " " << this->received_data_storage->get_light_spot_mod()[1];
    }

    if(this->received_data_storage->get_light_spot_mod()[0] > 0)
        ui->photoelectric_value_label_x_1->setText(QString("+"+QString::number(this->received_data_storage->get_light_spot_mod()[0])));
    else
        ui->photoelectric_value_label_x_1->setText(QString(""));

    if(this->received_data_storage->get_light_spot_mod()[1] > 0)
        ui->photoelectric_value_label_x_2->setText(QString("+"+QString::number(this->received_data_storage->get_light_spot_mod()[1])));
    else
        ui->photoelectric_value_label_x_2->setText(QString(""));

    if(this->received_data_storage->get_light_spot_mod()[2] > 0)
        ui->photoelectric_value_label_y_1->setText(QString("+"+QString::number(this->received_data_storage->get_light_spot_mod()[2])));
    else
        ui->photoelectric_value_label_y_1->setText(QString(""));

    if(this->received_data_storage->get_light_spot_mod()[3] > 0)
        ui->photoelectric_value_label_y_2->setText(QString("+"+QString::number(this->received_data_storage->get_light_spot_mod()[3])));
    else
        ui->photoelectric_value_label_y_2->setText(QString(""));
}
