#-------------------------------------------------
#
# Project created by QtCreator 2016-05-09T21:54:35
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SolarTrackerInterface
TEMPLATE = app


SOURCES += main.cpp\
    login_window.cpp \
    connected_pic_dialog.cpp \
    console.cpp \
    calibrating_engines.cpp \
    serial_messages.cpp \
    calibrating_light_sensors.cpp

HEADERS  += \
    login_window.h \
    connected_pic_dialog.h \
    console.h \
    calibrating_engines.h \
    serial_messages.h \
    calibrating_light_sensors.h

FORMS    += \
    login_window.ui \
    connected_pic_dialog.ui \
    calibrating_engines.ui \
    calibrating_light_sensors.ui

DISTFILES += \
    CMakeLists.txt


CONFIG+= static

RESOURCES += \
    myresources.qrc
