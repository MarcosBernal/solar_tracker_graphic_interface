#include "login_window.h"
#include "connected_pic_dialog.h"
#include "ui_login_window.h"
#include <stdio.h>
#include <QSerialPortInfo>
#include <QMessageBox>

LoginWindow::LoginWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::LoginWindow)
{
    ui->setupUi(this);

    // Example use QSerialPortInfo
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
         if (info.manufacturer() == "Prolific")
             this->port_UART = info;

    this->on_UART_restore_settings_clicked();
}

LoginWindow::~LoginWindow()
{
    delete ui;
}


void LoginWindow::on_UART_connect_clicked()
{
     if(this->port_UART.manufacturer() == "Prolific")
     {
         // Example use QSerialPort
         if(!this->ui->UART_speed->toPlainText().compare(QString::number(1200))            // If it is equal to 1200
                 ||!this->ui->UART_speed->toPlainText().compare(QString::number(2400)))   // or it is equal to 19200
         {
             this->serial.setBaudRate(this->ui->UART_speed->toPlainText().toInt());
             this->serial.setPort(port_UART);

             if (!this->serial.open(QIODevice::ReadWrite))
             {
                   QMessageBox::warning(this, "Connection Failed", this->serial.errorString());
                   return;
             }

             ConnectedPICDialog *window = new ConnectedPICDialog(this, &(this->port_UART), &(this->serial));
             qDebug() << "Name : " << this->port_UART.portName();
             qDebug() << "Description : " << this->port_UART.description();
             qDebug() << "Manufacturer : " << this->port_UART.manufacturer();
             this->setWindowOpacity(0);
             window->exec();
             this->setWindowOpacity(100);
        }
     }

     if(this->serial.isOpen())
          this->serial.close();
     else
     {
         QMessageBox::warning(this, "None connection found", "Please, check the parameter!!");
     }
     this->close();
}

void LoginWindow::on_UART_restore_settings_clicked()
{
    this->ui->UART_speed->setText(QString::number(2400));
    this->ui->UART_port->setText(this->port_UART.portName());
    this->ui->UART_data_bits->setText(QString::number(8));
    this->ui->UART_parity->setCurrentIndex(0);
}
