#ifndef CALIBRATING_ENGINES_H
#define CALIBRATING_ENGINES_H

#include <QDialog>
#include <QWidget>
#include <QDoubleSpinBox>
#include <QLCDNumber>

namespace Ui {
class CalibratingEngines;
}

class CalibratingEngines : public QDialog
{
    Q_OBJECT

public:
    explicit CalibratingEngines(QWidget *parent = 0);
    CalibratingEngines(QWidget *parent, float *, bool *);

    ~CalibratingEngines();

private slots:
    void on_apply_button_clicked();
    void on_ok_button_clicked();
    void on_cancel_button_clicked();
    void on_restore_button_clicked();

private:
    bool * changed_values;
    float *k_pid_constants;
    float k_pid_constants_backup[3];
    Ui::CalibratingEngines *ui;
    void refresf_values(void);

    QDoubleSpinBox * k1_spin;
    QDoubleSpinBox * k2_spin;
    QDoubleSpinBox * k3_spin;

    QLCDNumber * k1_lcd;
    QLCDNumber * k2_lcd;
    QLCDNumber * k3_lcd;
};

#endif // CALIBRATING_ENGINES_H
