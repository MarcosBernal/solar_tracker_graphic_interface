#ifndef CONNECTED_PIC_DIALOG_H
#define CONNECTED_PIC_DIALOG_H

#include <string.h>
#include <QDialog>
#include <QDebug>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QTextStream>
#include <QMessageBox>

#include "console.h"
#include "serial_messages.h"

namespace Ui {
class ConnectedPICDialog;
}

class ConnectedPICDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ConnectedPICDialog(QWidget *parent = 0);
    ConnectedPICDialog(QWidget *parent, QSerialPortInfo *info_conn, QSerialPort *serial_conn);
    ~ConnectedPICDialog();

    void writeDataSerial(const QByteArray &);
    void readDataSerial();
    void sort_message_bytes(unsigned char []);
    void manageDataSerial(QByteArray data);
    void manageSyncMessageStorage(SyncMessage message_serial);
    void manageAsynValuMessageStorage(AsynValuMessage message_serial);
    void manageAsynCodeMessageStorage(AsynCodeMessage message_serial);
    void manageAsynStateMessageStorage(AsynStateMessage message_serial);
    void handleError(QSerialPort::SerialPortError error);
    void closeSerialPort();
    void reseting_graphical_interface();

    void send_async_order(uint8_t order);
    void send_new_state_message(AsynStateMessage message);

private slots:
    void on_close_communication_button_clicked();

    void on_calibrate_k_pid_values_clicked();



    void on_search_light_spot_button_clicked();

    void on_track_light_spot_button_clicked();

    void on_manual_movement_reset_button_clicked();

    void on_manual_movement_init_button_clicked();

    void on_manual_movement_axis_y_slider_sliderReleased();

    void on_manual_movement_axis_x_slider_sliderReleased();

    void on_calibrate_photoelectric_value_clicked();

private:
    QSerialPort *serial;
    QSerialPortInfo *info;
    DataStorage* received_data_storage;
    Ui::ConnectedPICDialog *ui;
    Console *serial_output;
    bool serial_protocol_activated;


};

#endif // CONNECTED_PIC_DIALOG_H
