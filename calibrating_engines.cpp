#include "calibrating_engines.h"
#include "ui_calibrating_engines.h"

CalibratingEngines::CalibratingEngines(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CalibratingEngines)
{
    ui->setupUi(this);
}

CalibratingEngines::CalibratingEngines(QWidget *parent, float *constants, bool *changed_values) :
    QDialog(parent,Qt::WindowSystemMenuHint | Qt::WindowTitleHint), //| Qt::WindowCloseButtonHint), Removed the cross option from window
    changed_values(changed_values),
    k_pid_constants(constants),
    ui(new Ui::CalibratingEngines)
{
    ui->setupUi(this);

    this->setWindowTitle(QString("Calibrating Engines"));
    //this->setWindowFlags(Qt::WindowContextHelpButtonHint);

    k_pid_constants_backup[0] = k_pid_constants[0];
    k_pid_constants_backup[1] = k_pid_constants[1];
    k_pid_constants_backup[2] = k_pid_constants[2];
    k1_spin =  ui->doubleSpinBox_k1;
    k2_spin =  ui->doubleSpinBox_k2;
    k3_spin =  ui->doubleSpinBox_k3;
    k1_lcd = ui->kpidvalues_1;
    k2_lcd = ui->kpidvalues_2;
    k3_lcd = ui->kpidvalues_3;

    k1_spin->setRange(0.00, 1.00);
    k1_spin->setSingleStep(0.01);
    k1_spin->setValue(k_pid_constants[0]);

    k2_spin->setRange(0.00, 1.00);
    k2_spin->setSingleStep(0.01);
    k2_spin->setValue(k_pid_constants[1]);

    k3_spin->setRange(0.00, 1.00);
    k3_spin->setSingleStep(0.01);
    k3_spin->setValue(k_pid_constants[2]);

    refresf_values();
}

CalibratingEngines::~CalibratingEngines()
{
    delete ui;
}

void CalibratingEngines::on_apply_button_clicked()
{
   k_pid_constants[0] = k1_spin->value()*10;
   k_pid_constants[1] = k2_spin->value()*10;
   k_pid_constants[2] = k3_spin->value()*10;
   *changed_values = true;
   refresf_values();
}

void CalibratingEngines::on_ok_button_clicked()
{
  on_apply_button_clicked();
  close();
}

void CalibratingEngines::on_cancel_button_clicked()
{
  on_restore_button_clicked();
  close();
}

void CalibratingEngines::on_restore_button_clicked()
{
    k_pid_constants[0] = k_pid_constants_backup[0];
    k_pid_constants[1] = k_pid_constants_backup[1];
    k_pid_constants[2] = k_pid_constants_backup[2];
    k1_spin->setValue(k_pid_constants[0]);
    k2_spin->setValue(k_pid_constants[1]);
    k3_spin->setValue(k_pid_constants[2]);
    *changed_values = false;
    this->refresf_values();

}

void CalibratingEngines::refresf_values()
{
    k1_lcd->display(k_pid_constants[0]);
    k2_lcd->display(k_pid_constants[1]);
    k3_lcd->display(k_pid_constants[2]);
}
