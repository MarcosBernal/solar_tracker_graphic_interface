#ifndef LOGINWINDOW_H
#define LOGINWINDOW_H

#include <QMainWindow>
#include <QtSerialPort>

namespace Ui {
class LoginWindow;
}

class LoginWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit LoginWindow(QWidget *parent = 0);
    ~LoginWindow();

private slots:
    void on_UART_connect_clicked();

    void on_UART_restore_settings_clicked();

private:
    Ui::LoginWindow *ui;
    QSerialPortInfo port_UART;
    QSerialPort serial;
};

#endif // LOGINWINDOW_H
